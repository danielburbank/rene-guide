# Updating:
```bash
# Reset and clean repo, grab new manifest version, update sources
cd $BUILDBASE/android/lineage
repo forall -c 'git reset --hard'
repo forall -c 'git clean -dxf'
git -C .repo/local_manifests pull
repo sync --force-sync
source build/envsetup.sh

# Pick off Gerrit
repopick -t nvidia-enhancements-r
repopick -t nvidia-shieldtech-r
repopick -t icosa-bt-r
repopick -t nvidia-nvgpu-r
repopick 304329
repopick 302555

# Patch files
git -C device/nvidia/tegra-common am ../../../.repo/local_manifests/patches/device_nvidia_tegra-common-HAX.patch
git -C device/nvidia/t210-common am ../../../.repo/local_manifests/patches/device_nvidia_t210-common-uevent.patch
git -C frameworks/native am ../../.repo/local_manifests/patches/frameworks_native-joycon-filter.patch
git -C bionic am ../.repo/local_manifests/patches/bionic_intrinsics.patch
git -C build/soong am ../../.repo/local_manifests/patches/build_soong-build-HACK.patch
git -C device/nintendo/icosa_sr am ../../../.repo/local_manifests/patches/device_nintendo_icosa_sr_sensorhal.patch
git -C hardware/nintendo/joycond am ../../../.repo/local_manifests/patches/hardware_nintendo_joycond-Screenshot-Patch.patch

# Force software rendering
git -C device/nintendo/icosa_sr am ../../../.repo/local_manifests/patches/device_nintendo_icosa_sr-hwc.patch

# Use hardware rendering (OPTIONAL, NOT RECOMMENDED)
git -C device/nintendo/icosa_sr am ../../../.repo/local_manifests/patches/device_nintendo_icosa_sr-bring-back-hwc.patch

# Configure CCache
export USE_CCACHE=1
export CCACHE_EXEC=$(which ccache)
export WITHOUT_CHECK_API=true
ccache -M 50G

# For Android Mobile
lunch lineage_icosa_sr-userdebug 

# For Android TV
lunch lineage_icosa_tv_sr-userdebug 

# Build
make bacon
```

Go to `out/target/product/[device name]` (ex. `out/target/product/icosa_sr`) in your lineage source directory and copy `lineage-18.1-[date]-UNOFFICIAL-[device name].zip` to anywhere on your SD card.

If TWRP has been updated since you last flashed, download https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/twrp.img and put it in `switchroot/install` on your SD card and in hekate navigate to `Tools` -> `Partition SD Card` then `Flash Android`

Boot into TWRP by holding the volume up button while booting Android, once in TWRP navigate to `external_sd` and flash the zip you placed on your SD card earlier.
