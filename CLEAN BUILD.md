# Clean Build:
```bash
# Init repository and grab Switchroot 18.1 manifest
cd $BUILDBASE/android/lineage
repo init -u https://github.com/LineageOS/android.git -b lineage-18.1
git clone https://gitlab.com/makinbacon17/manifest.git -b lineage-18.1 .repo/local_manifests
repo sync
source build/envsetup.sh

# The steps below must be done in order, otherwise either repo picking or building will fail

# Pick off Gerrit
repopick -t nvidia-enhancements-r
repopick -t nvidia-shieldtech-r
repopick -t icosa-bt-r
repopick -t nvidia-nvgpu-r
repopick 304329
repopick 302555

# Patch files, if any of these directories come up as not found make sure to do repo sync again with repo sync -f
git -C device/nvidia/tegra-common am ../../../.repo/local_manifests/patches/device_nvidia_tegra-common-HAX.patch
git -C device/nvidia/t210-common am ../../../.repo/local_manifests/patches/device_nvidia_t210-common-uevent.patch
git -C frameworks/native am ../../.repo/local_manifests/patches/frameworks_native-joycon-filter.patch
git -C bionic am ../.repo/local_manifests/patches/bionic_intrinsics.patch
git -C build/soong am ../../.repo/local_manifests/patches/build_soong-build-HACK.patch
git -C device/nintendo/icosa_sr am ../../../.repo/local_manifests/patches/device_nintendo_icosa_sr_sensorhal.patch
git -C hardware/nintendo/joycond am ../../../.repo/local_manifests/patches/hardware_nintendo_joycond-Screenshot-Patch.patch

#IMPORTANT, if you want to apply the use hardware rendering patch you must apply both patches below
# Force software rendering
git -C device/nintendo/icosa_sr am ../../../.repo/local_manifests/patches/device_nintendo_icosa_sr-hwc.patch

# Use hardware rendering (OPTIONAL, NOT RECOMMENDED, BREAKS DOCKING FOR NOW)
git -C device/nintendo/icosa_sr am ../../../.repo/local_manifests/patches/device_nintendo_icosa_sr-bring-back-hwc.patch



# Configure CCache
export USE_CCACHE=1
export CCACHE_EXEC=$(which ccache)
export WITHOUT_CHECK_API=true
ccache -M 50G

# For Android Mobile
lunch lineage_icosa_sr-userdebug 

# For Android TV
lunch lineage_icosa_tv_sr-userdebug 

# Build 
make bacon
```

Download hekate and yeet it's contents on your SD card.

Put https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/00-android.ini?inline=false in `/bootloader/ini` on your SD card.

Go to `out/target/product/[device name]` (ex. `out/target/product/icosa_sr`) in your lineage source directory and copy `lineage-18.1-[date]-UNOFFICIAL-[device name].zip` to anywhere on your SD card.

Copy `boot.img` and `install/tegra210-icosa.dtb` from that same directory to `/switchroot/install` on your SD card.

Download https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/twrp.img and copy it to `/switchroot/install`.

Launch Hekate and navigate to `Tools` -> `Partition SD Card`.

You can use the `Android ` slider to choose how much storage you want to give to Android, then press `Next Step`, then `Start`.

Once that finishes press `Flash Android`, then `Continue`.

Press `Continue` again and it should reboot to TWRP.

If it doesn't reboot to TWRP, hold the power button for 12 seconds, boot into hekate again, then select the Android config while holding VOL-UP and hold VOL-UP until it gets into TWRP.

In TWRP tap mount, then check any/every partition you can, if some/all cannot be selected, just continue.

Now go back and press `Install` then navigate to `external_sd` then to wherever you put `lineage-18.1-[date]-UNOFFICIAL-[device name].zip` and tap on it.

Swipe to confirm and it should install the rest of Android.

Now just reboot, load hekate again, select the Android config, and hope it boots.
