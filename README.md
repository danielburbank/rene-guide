# Rene-Guide
Switchroot Android R is NOT RECOMMENDED OR SUPPORTED FOR ACTUAL USE. This is for TESTING PURPOSES ONLY and can result in damage to your device--continue at your own risk.

Credit to the man himself, @ZachyCatGames, for making the Q-Tips-Guide this was forked from.

Credit to the LineageOS Wiki `foster` build instructions (https://wiki.lineageos.org/devices/foster/build) for initial steps.

IMPORTANT: `-j[number]` refers to the number of simultaneous connections allowed on `repo sync` and `make bacon`. Less RAM, less hardware threads --> use a smaller number. Using a higher number might make troubleshooting difficult as the process will finish the unaffected jobs before breaking, leaving the error far behind.

# Instructions
Follow [ENVIRONMENT.md](ENVIRONMENT.md) for first build to set up your build environment

Follow [CLEAN BUILD.md](CLEAN BUILD.md) for clean builds

Follow [UPDATING.md](UPDATING.md) for subsequent builds
